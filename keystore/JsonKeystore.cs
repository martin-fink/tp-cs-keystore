﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace keystore
{
    class JsonKeystore : Keystore
    {
        protected string savePath;
        protected Dictionary<string, string> items;

        public JsonKeystore(string savePath)
        {
            this.savePath = savePath;
            if (File.Exists(savePath))
            {
                var content = File.ReadAllText(savePath);
                items = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
            }
            else
            {
                items = new Dictionary<string, string>();
            }
        }

        public override bool Delete(string key)
        {
            if (!items.ContainsKey(key)) return false;

            items.Remove(key);
            SaveContents();
            return true;
        }

        public override List<Tuple<string, string>> Find(string needle)
        {
            List<Tuple<string, string>> matches = new List<Tuple<string, string>>();

            foreach (var key in items.Keys)
            {
                if (items[key].Contains(needle))
                {
                    matches.Add(new Tuple<string, string>(key, items[key]));
                }
            }

            return matches;
        }

        public override string Get(string key)
        {
            return items.TryGetValue(key, out string value) ? value : null;
        }

        public override void Put(string key, string value)
        {
            items[key] = value;
            SaveContents();
        }

        protected void SaveContents()
        {
            var content = JsonConvert.SerializeObject(items);
            using (var w = new StreamWriter(savePath, false))
            {
                w.Write(content);
            }
        }
    }
}
