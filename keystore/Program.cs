﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace keystore
{
    internal class Program
    {
        private static Keystore _keystore;

        private static void Main(string[] args)
        {
            var env = Environment.GetEnvironmentVariable("QUERY_STRING");

            if (env == null)
            {
                Console.WriteLine("Program must be called from cgi.");
                Environment.Exit(1);
            }

            Console.WriteLine("Content-Type: text/plain\n\n");

            var commands = new Dictionary<string, string>();

            var sets = env.Split('&');
            
            foreach (var set in sets)
            {
                var kv = set.Split('=');

                commands[kv[0]] = kv[1];
            }

            if (commands.ContainsKey("cmd"))
            {
                var cmd = commands["cmd"];

                try
                {
                    _keystore = new JsonKeystore("keystore.json");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                switch (cmd)
                {
                    case "put":
                        PutString(commands);
                        break;
                    case "get":
                        GetString(commands);
                        break;
                    case "find":
                        FindString(commands);
                        break;
                    case "del":
                        DeleteString(commands);
                        break;
                    default:
                        ExitWithMessage(1, "Unknown command '" + cmd + "'");
                        break;
                }

                Environment.Exit(0);
            }
            else
            {
                ExitWithMessage(1, "param 'cmd' is required.");
            }
        }

        private static void PutString(IReadOnlyDictionary<string, string> args)
        {
            if (!args.ContainsKey("key") || !args.ContainsKey("val"))
            {
                ExitWithMessage(1, "'put' expects params 'key' and 'val'");
            }

            var key = args["key"];
            var val = args["val"];

            _keystore.Put(key, val);
        }

        private static void GetString(IReadOnlyDictionary<string, string> args)
        {
            if (!args.ContainsKey("key"))
            {
                ExitWithMessage(1, "param 'key' not found");
            }

            var key = args["key"];

            var value = _keystore.Get(key);

            if (value != null)
            {
                Console.WriteLine(key + ": " + value);
            }
            else
            {
                ExitWithMessage(2, "key not found");
            }
        }

        private static void FindString(IReadOnlyDictionary<string, string> args)
        {
            if (!args.ContainsKey("val"))
            {
                ExitWithMessage(1, "param 'val' not found");
            }

            var val = args["val"];

            var values = _keystore.Find(val);


            foreach (var tuple in values)
            {
                Console.WriteLine(tuple.Item1 + ": " + tuple.Item2);
            }
        }

        private static void DeleteString(IReadOnlyDictionary<string, string> args)
        {
            if (!args.ContainsKey("key"))
            {
                ExitWithMessage(1, "param 'key' not found");
            }

            var key = args["key"];

            if (!_keystore.Delete(key))
            {
                ExitWithMessage(2, "key not found");
            }
        }

        private static void ExitWithMessage(int exitCode, object obj)
        {
            var msg = JsonConvert.SerializeObject(obj);
            Console.WriteLine(msg);
            Environment.Exit(exitCode);
        }
    }
}
