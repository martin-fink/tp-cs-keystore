﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace keystore
{
    abstract class Keystore
    {

        public abstract void Put(string key, string value);

        public abstract string Get(string key);

        public abstract List<Tuple<string, string>> Find(string needle);

        public abstract bool Delete(string key);
    }
}
